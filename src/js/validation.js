/* eslint-disable */
const regexBank = {
    email: '^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$',
    nameSurname: '^[A-Za-z]{0,20}$',
    message: '^[A-Za-z]{0,200}$',
};

const validateInput = (name, content) => {
    const particularRegex = new RegExp(regexBank[name]);
    return particularRegex.test(content);
};

export { validateInput };
