import { NumToWords } from '../helpers';

const navigation = {
    init() {
        navigation.scrollPosition = 0;
        navigation.sectionsAttributes = [];
        navigation.currentLineClass = '';
        navigation.handleHamburgerIcon();
        navigation.handleLinksClickBehaviour();
        navigation.fillSectionAttributes();
        // navigation.setCurrentLanguage();
        window.addEventListener('scroll', () => {
            navigation.stickyNav();
            navigation.scrollSpyToggling();
        });
    },

    handleHamburgerIcon() {
        const hamburgerButton = document.querySelector('.hamburger');
        hamburgerButton.addEventListener('click', navigation.toggleNavigation);
    },

    handleLinksClickBehaviour() {
        const wrapper = document.querySelector('.navigation');
        const links = wrapper.querySelectorAll('.navigation-item');
        const logoAnchor = document.querySelector('.logo a');
        const contactBtn = document.querySelector('.documents-btn');
        links.forEach(el => {
            navigation.hideOnNavClick(el);
            el.addEventListener('click', e => {
                navigation.smoothScrolling(e, el);
            });
        });
        contactBtn.addEventListener('click', e => {
            navigation.smoothScrolling(e, contactBtn);
        });
        logoAnchor.addEventListener('click', e => {
            navigation.smoothScrolling(e, logoAnchor);
        });
    },

    toggleNavigation() {
        const hamburgerButton = document.querySelector('.hamburger');
        const wrapper = document.querySelector('.navigation');
        hamburgerButton.classList.toggle('open');
        wrapper.classList.toggle('active');
        document.body.classList.toggle('overlay');
    },

    hideOnNavClick(el) {
        el.addEventListener('click', () => {
            navigation.toggleNavigation();
        });
    },

    stickyNav() {
        setInterval(() => {
            const wrapper = document.querySelector('nav');
            const navWrapper = document.querySelector('.nav-wrapper');
            const header = document.querySelector('header');
            if (window.pageYOffset >= wrapper.offsetTop + 100) {
                if (!header.classList.contains('sticky')) {
                    header.classList.add('sticky');
                    wrapper.classList.add('sticky');
                    navWrapper.classList.add('sticky');
                }
            } else if (header.classList.contains('sticky')) {
                wrapper.classList.remove('sticky');
                header.classList.remove('sticky');
                navWrapper.classList.remove('sticky');
            }
        }, 100);
    },

    hideShowOnScroll() {
        setInterval(() => {
            if (window.innerWidth < 768) {
                const windowY = window.scrollY;
                const nav = document.querySelector('nav');
                if (windowY < navigation.scrollPosition) {
                    // Scrolling UP
                    nav.classList.remove('toggleTop');
                }
                if (windowY > navigation.scrollPosition && !nav.classList.contains('toggleTop') && windowY > 150) {
                    // Scrolling DOWN
                    nav.classList.add('toggleTop');
                }
                navigation.scrollPosition = windowY;
            }
        }, 1000);
    },

    fillSectionAttributes() {
        const sections = document.querySelectorAll('section');
        sections.forEach(el =>
            navigation.sectionsAttributes.push({
                id: el.id,
                component: el,
            }),
        );
    },

    scrollSpyToggling() {
        const options = {
            root: null,
            rootMargin: '0px',
            threshold: 0.5, // visible amount of item shown in relation to root
        };

        navigation.observer = new IntersectionObserver(navigation.handleObserver, options);
        const elements = document.querySelectorAll('section');
        elements.forEach(el => navigation.observer.observe(el));
    },

    handleObserver(entries) {
        entries.forEach(el => {
            const href = `${el.target.getAttribute('id')}`;
            const target = document.querySelector(`[data-target="${href}"]`);
            if (el.isIntersecting && el.intersectionRatio >= 0.5) {
                target.classList.add('active');
                navigation.clearUnnecessaryActiveLinks();
                navigation.changeLinePosition();
            } else {
                target.classList.remove('active');
                navigation.changeLinePosition();
            }
        });
    },

    clearUnnecessaryActiveLinks() {
        const activeElements = document.querySelectorAll('.navigation-item.active');
        for (let i = 0; i < activeElements.length - 1; i += 1) {
            activeElements[i].classList.remove('active');
        }
    },

    changeLinePosition() {
        const elements = document.querySelectorAll('.navigation-item');
        let index = '';
        elements.forEach((el, i) => {
            if (el.classList.contains('active')) {
                index = i + 1;
            }
        });
        const line = document.querySelector('.navLine');
        const transformedLength = NumToWords(index) || '';

        if (navigation.currentLineClass !== '') {
            line.classList.remove(navigation.currentLineClass);
        }

        if (transformedLength) {
            line.classList.add(transformedLength);
            navigation.currentLineClass = transformedLength;
        }
    },

    smoothScrolling(e, respond = null) {
        e.preventDefault();
        const distanceToTop = el => Math.floor(el.getBoundingClientRect().top - 250);
        const targetID = respond ? respond.getAttribute('href') : this.getAttribute('href');
        const targetAnchor = document.querySelector(targetID);
        if (!targetAnchor) return;
        const originalTop = distanceToTop(targetAnchor);
        const dataOffset = parseInt(targetAnchor.getAttribute('data-scroll-offset'), 10) || 0;
        window.scrollBy({ top: originalTop - dataOffset, left: 0, behavior: 'smooth' });
        const checkIfDone = setInterval(() => {
            const atBottom = window.innerHeight + window.pageYOffset >= document.body.offsetHeight - 2;
            if (distanceToTop(targetAnchor) === -250 || atBottom) {
                targetAnchor.tabIndex = '-1';
                window.history.pushState('', '', targetID);
                clearInterval(checkIfDone);
            }
        }, 100);
    },

    setCurrentLanguage() {
        const wrapper = document.querySelector('.languages');
        const languages = wrapper.querySelectorAll('a');
        languages.forEach(el => {
            if (window.location.href === el.href) {
                el.parentNode.classList.add('active');
            }
        });
    },
};
export default navigation;
