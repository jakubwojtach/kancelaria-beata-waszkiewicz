import { FormDataObject } from '../helpers';
import { validateInput } from '../validation';

const contact = {
    init() {
        const forms = document.querySelectorAll('form');
        forms.forEach(el => contact.submitFormIntoMailClient(el));
    },

    submitFormIntoMailClient(el) {
        el.addEventListener('submit', e => {
            e.preventDefault();
            const formData = FormDataObject(el);
            const FormDataValidation = contact.prepareValidationData(el);

            if (!FormDataValidation.validations.includes(false)) {
                contact.cleanValidationClasses();
                contact.sendMessage(contact.prepareMessage(formData), el);
            } else {
                contact.setValidationClasses(FormDataValidation.data);
            }
        });
    },

    prepareValidationData(el) {
        const formData = FormDataObject(el);
        const values = Object.values(formData);
        const data = values.map(element => ({
            element,
            validate: validateInput(element.name, element.value),
        }));
        return {
            data,
            validations: data.map(element => element.validate),
        };
    },

    prepareMessage(formData) {
        // Form Data creation
        const target = 'mailto:waszkiewicz@notariusze.waw.pl';
        const subject = "?subject=Spotkajmy Się! / Let's Collaborate!";
        const body = `&body=Imię i Nazwisko / Name and Surname:${formData.nameSurname.value}
                ${escape('\n')}Email:${formData.email.value}
                ${escape('\n')}Wiadomość / Message: ${formData.message.value}`;
        return `${target}${subject}${body}`;
    },

    sendMessage(mailtoContent, formToReset) {
        formToReset.classList.add('loading');
        setTimeout(() => {
            // Creating element which click is simulated
            const mailElement = document.createElement('a');
            mailElement.id = 'mailElement';
            mailElement.href = mailtoContent;
            document.body.appendChild(mailElement);
            mailElement.click();

            // Reset form
            formToReset.reset();

            // Remove element after all
            document.getElementById('mailElement').parentNode.removeChild(document.getElementById('mailElement'));
            formToReset.classList.remove('loading');
        }, 2000);
    },

    cleanValidationClasses() {
        document.querySelectorAll('input').forEach(input => input.classList.remove('invalid'));
    },

    setValidationClasses(validateObject) {
        const invalidInputs = validateObject.filter(element => !element.validate);
        contact.cleanValidationClasses();
        invalidInputs.map(element => {
            const inputToMention = document.querySelector(`input[name="${element.element.name}"]`);
            inputToMention.classList.add('invalid');
        });
    },
};
export default contact;
