const popover = {
    init() {
        popover.togglePopover();
    },

    togglePopover() {
        const elements = document.querySelectorAll('.popover-icon');
        elements.forEach(el =>
            el.addEventListener('click', () => {
                el.parentNode.classList.toggle('active');
            }),
        );
    },
};
export default popover;
