import { WebpMachine } from 'webp-hero';

const polyfill = () => {
    const webpMachine = new WebpMachine();
    const sliderImagesCont = document.querySelector('.slider-images');
    const sliderImages = sliderImagesCont.querySelectorAll('img');
    const partnersCont = document.querySelector('.partners-list');
    const partnersImages = partnersCont.querySelectorAll('img');

    sliderImages.forEach(el => webpMachine.polyfillImage(el));
    partnersImages.forEach(el => webpMachine.polyfillImage(el));
};

export { polyfill };
