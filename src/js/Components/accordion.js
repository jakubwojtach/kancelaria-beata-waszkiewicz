const accordion = {
    init() {
        const containers = document.querySelectorAll('.accordion__wrapper');
        containers.forEach(el => accordion.prepareContainer(el));
    },

    prepareContainer(container) {
        const items = container.querySelectorAll('.accordion-item');
        items.forEach(el => {
            const element = el.querySelector('.accordion-item__title');
            element.addEventListener('click', e => {
                accordion.handleClick(el, container, e);
            });
        });
    },

    handleClick(element, parentElement, event) {
        // console.log(event);
        if (!event.srcElement.classList.contains('popover-icon')) {
            if (!parentElement.classList.contains('multiple')) {
                if (element.classList.contains('active')) {
                    element.classList.remove('active');
                    return;
                }
                const items = parentElement.querySelectorAll('.accordion-item');
                items.forEach(el => el.classList.remove('active'));
            }
            element.classList.toggle('active');
        }
    },
};

export default accordion;
