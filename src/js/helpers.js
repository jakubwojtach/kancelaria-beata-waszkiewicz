const NumToWords = number => {
    // eslint-disable-next-line max-len
    const first = ['', 'one ', 'two ', 'three ', 'four ', 'five ', 'six ', 'seven ', 'eight ', 'nine ', 'ten ', 'eleven ', 'twelve ', 'thirteen ', 'fourteen ', 'fifteen ', 'sixteen ', 'seventeen ', 'eighteen ', 'nineteen '];
    const tens = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
    const mad = ['', 'thousand', 'million', 'billion', 'trillion'];
    let word = '';

    for (let i = 0; i < mad.length; i += 1) {
        let tempNumber = number % (100 * Math.pow(1000, i));
        if (Math.floor(tempNumber / Math.pow(1000, i)) !== 0) {
            if (Math.floor(tempNumber / Math.pow(1000, i)) < 20) {
                word = `${first[Math.floor(tempNumber / Math.pow(1000, i))] + mad[i]} ${word}`;
            } else {
                // eslint-disable-next-line max-len
                word = `${tens[Math.floor(tempNumber / (10 * Math.pow(1000, i)))]}-${first[Math.floor(tempNumber / Math.pow(1000, i)) % 10]}${mad[i]} ${word}`;
            }
        }

        tempNumber = number % (Math.pow(1000, i + 1));
        if (Math.floor(tempNumber / (100 * Math.pow(1000, i))) !== 0) {
            word = `${first[Math.floor(tempNumber / (100 * Math.pow(1000, i)))]} hunderd ${word}`;
        }
    }
    return word.trim();
};

/*!
 * Serialize all form data into a query string
 * (c) 2018 Chris Ferdinandi, MIT License, https://gomakethings.com
 * @param  {Node}   form The form to serialize
 * @return {String}      The serialized form data
 */
const Serialize = form => {
    // Setup our serialized data
    const serialized = [];

    // Loop through each field in the form
    for (let i = 0; i < form.elements.length; i += 1) {
        const field = form.elements[i];
        if (field.name
            && !field.disabled && field.type !== 'file'
            && field.type !== 'reset' && field.type !== 'submit'
            && field.type !== 'button') {
            // If a multi-select, get all selections
            if (field.type === 'select-multiple') {
                for (let n = 0; n < field.options.length; n += 1) {
                    if (field.options[n].selected) {
                        serialized.push(
                            `${encodeURIComponent(field.name)}=${encodeURIComponent(field.options[n].value)}`
                        );
                    }
                }
            } else if ((field.type !== 'checkbox' && field.type !== 'radio') || field.checked) {
                serialized.push(`
                    ${encodeURIComponent(field.name)}=${encodeURIComponent(field.value)}`);
            }
        }
    }
    return serialized.join('&');
};

const ElementIsNotForbidden = (range, element) => !range.includes(element);

const FormDataObject = form => {
    // Setup our serialized data
    const formDataObject = [];
    const forbiddenRange = ['submit', 'file', 'reset', 'button'];
    // Loop through each field in the form
    for (let i = 0; i < form.elements.length; i += 1) {
        const field = form.elements[i];
        if (ElementIsNotForbidden(forbiddenRange, field.type)) {
            formDataObject[field.name] = {
                name: field.name,
                type: field.type,
                value: field.value,
            };
        }
    }
    return formDataObject;
};

export {
    NumToWords,
    Serialize,
    FormDataObject,
    ElementIsNotForbidden,
};
