import 'babel-polyfill';
import 'intersection-observer';
import { WebpMachine } from 'webp-hero';
import navigation from './Components/nav';
import contact from './Components/contact';
import accordion from './Components/accordion';
import popover from './Components/popover';

document.addEventListener('DOMContentLoaded', () => {
    const webpMachine = new WebpMachine();
    webpMachine.polyfillDocument();
    navigation.init();
    accordion.init();
    popover.init();
    contact.init();
});
