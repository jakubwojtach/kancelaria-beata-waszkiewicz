import gulp from 'gulp';
import babelify from 'babelify';
import imageMin from 'gulp-imagemin';
import browserify from 'browserify';
import connect from 'gulp-connect';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import sass from 'gulp-sass';
import notify from 'gulp-notify';
import plumber from 'gulp-plumber';
import cssMin from 'gulp-minify-css';
import minifyHTML from 'gulp-minify-html';
import pug from 'gulp-pug';
import scssLint from 'gulp-sass-lint';
import autoPrefix from 'gulp-autoprefixer';
import cache from 'gulp-cached';
import rename from 'gulp-rename';
import size from 'gulp-size';
import uglify from 'gulp-uglify';
import watch from 'gulp-watch';
import webP from 'gulp-webp';

// eslint-disable-next-line func-names
const onError = function(err) {
    notify.onError({
        title: 'Gulp',
        subtitle: 'Failure',
        message: 'Error: <%= error.message %>',
        sound: 'Beep',
    })(err);
    this.emit('end');
};

// ES6 transpilation task ✅
gulp.task('build', () =>
    browserify({
        entries: ['./src/js/index.js'],
    })
        .transform(
            babelify.configure({
                presets: ['es2015'],
            }),
        )
        .bundle()
        .pipe(
            plumber({
                errorHandler: onError,
            }),
        )
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(gulp.dest('./public'))
        .pipe(connect.reload()),
);

// server task ✅
gulp.task('startServer', () => {
    connect.server({
        root: './public',
        livereload: true,
        port: 9001,
        name: 'Website',
    });
});

// sass compilation task ✅
gulp.task('scss', () =>
    gulp
        .src('src/scss/main.scss')
        .pipe(
            plumber({
                errorHandler: onError,
            }),
        )
        .pipe(sass())
        .pipe(
            size({
                gzip: true,
                showFiles: true,
            }),
        )
        .pipe(
            autoPrefix({
                browsers: ['last 10 versions', 'ie 9', 'ie 9', 'ie 11'],
                cascade: false,
            }),
        )
        .pipe(rename('main.css'))
        .pipe(gulp.dest('public/css'))
        .pipe(cssMin())
        .pipe(
            size({
                gzip: true,
                showFiles: true,
            }),
        )
        .pipe(
            rename({
                suffix: '.min',
            }),
        )
        .pipe(gulp.dest('public/css')),
);

// sass lint task ✅
gulp.task('scss-lint', () => {
    gulp.src('src/scss/**/*.scss')
        .pipe(
            plumber({
                errorHandler: onError,
            }),
        )
        .pipe(cache('scssLint'))
        .pipe(scssLint());
});

// html minification task ✅
gulp.task('minify-html', () => {
    const opts = {
        comments: true,
        spare: true,
    };
    gulp.src('public/html/**/*.html')
        .pipe(
            plumber({
                errorHandler: onError,
            }),
        )
        .pipe(minifyHTML(opts))
        .pipe(gulp.dest('public'));
});

// PUG templates transform task ✅
gulp.task('transform-pug', () => {
    gulp.src('src/html/pages/*.pug')
        .pipe(
            plumber({
                errorHandler: onError,
            }),
        )
        .pipe(pug())
        .pipe(gulp.dest('public'));
});

// images optimization ✅
gulp.task('images', () => {
    gulp.src('src/img/**/*')
        .pipe(
            plumber({
                errorHandler: onError,
            }),
        )
        .pipe(
            imageMin([
                imageMin.svgo({
                    plugins: [
                        {
                            removeViewBox: true,
                        },
                    ],
                }),
                imageMin.optipng({ optimizationLevel: 2 }),
                imageMin.jpegtran({ progressive: true }),
            ]),
        )
        .pipe(
            webP({
                quality: 100,
                alphaQuality: 100,
            }),
        )
        .pipe(gulp.dest('public/img'));
});

// watch for all the files changes ✅
gulp.task('watch', () => {
    gulp.watch('src/scss/**/**', ['scss']);
    gulp.watch('src/scss/**/**', ['scss-lint']);
    watch('src/scss/**/**').pipe(connect.reload());
    gulp.watch('public/*.html', ['minify-html']);
    watch('public/*.html').pipe(connect.reload());
    gulp.watch('src/html/**/*.pug', ['transform-pug']);
    watch('src/html/*.pug').pipe(connect.reload());
    gulp.watch('src/img/**/**', ['images']);
    watch('src/img/**/**').pipe(connect.reload());
    gulp.watch('src/js/**/*.js', ['build']);
});

gulp.task('copyFonts', () => {
    gulp.src('src/fonts/**/*.{ttf,woff,eof,svg,woff2,otf}').pipe(gulp.dest('public/fonts'));
});

gulp.task('copyFavicon', () => {
    gulp.src('src/favicon.ico').pipe(gulp.dest('public'));
});

gulp.task('copyBg', () => {
    gulp.src('src/img/background.png').pipe(gulp.dest('public/img'));
});

// ✅ ✅ ✅ ✅ ✅ ✅ ✅

gulp.task('default', [
    'build',
    'scss',
    'scss-lint',
    'minify-html',
    'transform-pug',
    'images',
    'copyFonts',
    'watch',
    'startServer',
    'copyFavicon',
    'copyBg',
]);
